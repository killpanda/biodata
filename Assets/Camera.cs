﻿using UnityEngine;
using System.Collections;

public class Camera : MonoBehaviour {

	float speed = 0.0001f;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey(KeyCode.D)) {
			float xAxisValue = Input.GetAxis("Horizontal");
			float zAxisValue = Input.GetAxis("Vertical");
			if(UnityEngine.Camera.current != null)
			{
				UnityEngine.Camera.current.transform.Translate(new Vector3(xAxisValue+=speed, 0.0f, zAxisValue));
			}
		}
		if (Input.GetKey(KeyCode.A)) {
			float xAxisValue = Input.GetAxis("Horizontal");
			float zAxisValue = Input.GetAxis("Vertical");
			if(UnityEngine.Camera.current != null)
			{
				UnityEngine.Camera.current.transform.Translate(new Vector3(xAxisValue-=speed, 0.0f, zAxisValue));
			}
		}
		if (Input.GetKey(KeyCode.W)) {
			float xAxisValue = Input.GetAxis("Horizontal");
			float zAxisValue = Input.GetAxis("Vertical");
			if(UnityEngine.Camera.current != null)
			{
				UnityEngine.Camera.current.transform.Translate(new Vector3(xAxisValue, 0.0f, zAxisValue+=speed));
			}
		}
		if (Input.GetKey(KeyCode.S)) {
			float xAxisValue = Input.GetAxis("Horizontal");
			float zAxisValue = Input.GetAxis("Vertical");
			if(UnityEngine.Camera.current != null)
			{
				UnityEngine.Camera.current.transform.Translate(new Vector3(xAxisValue, 0.0f, zAxisValue-=speed));
			}
		}
		Quaternion temp = transform.rotation;
		if (Input.GetKey(KeyCode.RightArrow)) {
			if(UnityEngine.Camera.current != null)
			{
				//Camera.current.transform.Rotate(Quaternion.Euler(0f,45f,0f));
				temp.y += 0.01F;
				transform.rotation = temp;
			}
		}
		if (Input.GetKey(KeyCode.LeftArrow)) {
			if(UnityEngine.Camera.current != null)
			{
				//Camera.current.transform.Rotate(Quaternion.Euler(0f,45f,0f));
				temp.y -= 0.01F;
				transform.rotation = temp;
			}
		}
		if (Input.GetKey(KeyCode.DownArrow)) {
			if(UnityEngine.Camera.current != null)
			{
				//Camera.current.transform.Rotate(Quaternion.Euler(0f,45f,0f));
				temp.x += 0.01F;
				transform.rotation = temp;
			}
		}
		if (Input.GetKey(KeyCode.UpArrow)) {
			if(UnityEngine.Camera.current != null)
			{
				//Camera.current.transform.Rotate(Quaternion.Euler(0f,45f,0f));
				temp.x -= 0.01F;
				transform.rotation = temp;
			}
		}
		if (Input.GetKey(KeyCode.E)) {
			if(UnityEngine.Camera.current != null)
			{
				//Camera.current.transform.Rotate(Quaternion.Euler(0f,45f,0f));
				temp.z += 0.01F;
				transform.rotation = temp;
			}
		}
		if (Input.GetKey(KeyCode.F)) {
			if(UnityEngine.Camera.current != null)
			{
				//Camera.current.transform.Rotate(Quaternion.Euler(0f,45f,0f));
				temp.z -= 0.01F;
				transform.rotation = temp;
			}
		}


	}
}
