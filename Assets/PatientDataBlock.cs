﻿using UnityEngine;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using AssemblyCSharp;
//using AssemblyCSharp.Patient;

public class PatientDataBlock : MonoBehaviour {

	public GameObject corrdinateCube;
	static string connectionString=
		"Server=www.mina-asad.com,1433;" +
			"Database=SampleXYZDDB;" +
			"User ID=sdb_user;" +
			"Password=sdb_password;";
	static SqlConnection cn = new SqlConnection(connectionString);
	static string sCommand = "SELECT * FROM processedPatientData";
	SqlCommand myCommand = new SqlCommand (sCommand, cn);
	System.Collections.Generic.List<Patient> extractedPatientDataSet;

	String selectedPatientID;
	String selectedPatientGender;
	
	// Use this for initialization
	void Start () {
	
	}

	void OnGUI () {
		GUI.Box(new Rect (10, 60, 150, 25), "Patient ID: " + selectedPatientID);
		GUI.Box (new Rect (10, 85, 150, 25), "Patient Male: " + selectedPatientGender);
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.B)) {
			ReadData ();
				
			foreach (Patient data in extractedPatientDataSet) {
			
				Vector3 coordinateLocation = new Vector3 (data.Position_X, data.Position_Y, data.Position_Z);
				GameObject coordinateCubeObject = (GameObject)Instantiate (GameObject.CreatePrimitive (PrimitiveType.Cube),
			                                                     coordinateLocation,
			                                                     Quaternion.identity);

				if ( data.Gender==GenderType.male)
					coordinateCubeObject.renderer.material.color = new Color (0, 1, 0, 0.2F);
				else
					coordinateCubeObject.renderer.material.color = new Color (1, 0, 0, 0.2F);

				//coordinateCubeObject.tag = "CubeLivePatient";
				coordinateCubeObject.name = data.Patient_ID;
				// half size
				coordinateCubeObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
				coordinateCubeObject.AddComponent("PatientDetail");
			}
		}

		// select object
		if (Input.GetMouseButton(0)){
			RaycastHit hitInfo = new RaycastHit();
			bool hit = Physics.Raycast(UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

			if (hit) {
				foreach (Patient data in extractedPatientDataSet) {
					if (data.Patient_ID.Equals(hitInfo.transform.gameObject.name)) {

						selectedPatientID = data.Patient_ID;

						if (data.Gender == GenderType.male)
							selectedPatientGender = "Male";
						else
							selectedPatientGender = "Female";

						//hitInfo.transform.gameObject.renderer.material.color = new Color(1, 0, 0, 0.2F);
					}
				}
			}
		}
	}

	// helper

	void ReadData(){
		// read data and put data into list
		cn.Open ();
		SqlDataReader dataReader = myCommand.ExecuteReader ();
		extractedPatientDataSet = new System.Collections.Generic.List<Patient>();
		while (dataReader.Read()) {
			Patient extractedPatientData = new Patient(float.Parse(dataReader[1].ToString())/50.0f,
			                                           float.Parse(dataReader[2].ToString())/50.0f,
			                                           float.Parse(dataReader[3].ToString())/50.0f);
			extractedPatientData.Patient_ID = dataReader[0].ToString();
			if(dataReader[6].ToString().Equals("Male")) {
				extractedPatientData.Gender = GenderType.male;
			}
			else {
				extractedPatientData.Gender = GenderType.female;
			}
			extractedPatientDataSet.Add(extractedPatientData);
		}
		cn.Close ();
	}
}











































